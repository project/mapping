
/**
 * @file
 * Main JS file for the mapping architecture
 * module.  This kicks off the JS processing.
 */

(function ($, Drupal) {

  /**
   * Initial behavior to start the map processing.
   * The data for the mapping is stored in the jQuery.data()
   * storage mechanism.
   */
  Drupal.behaviors.mapping = {
    'attach': function (context, settings) {
      $(context).find('.mapping-map').each(function () {
        var mapId = this.id;
        if (mapId && settings.mapping.maps[mapId]) {
          Drupal.mapping.attachBehaviors(this, settings.mapping.maps[mapId]);
        }
      });
    }
  };

  /**
   * Object to store supplemental functions.
   */
  Drupal.mapping = {
    /**
     * Object to store layer handlers.
     */
    'layerHandlers': {},

    behaviors: {
      initMap: {
        attach: function (map, settings) {
          var renderMap = Drupal.mapping.renderMap(settings);

          // Attach data to map DOM object
          $(map).data('mapping', {
            'data': settings,
            'object': renderMap
          });
        }
      }
    },

    attachBehaviors: function (map, mapSettings) {
      var i, errors = [], behaviors = Drupal.mapping.behaviors;
      // Execute all of them.
      for (i in behaviors) {
        if (behaviors.hasOwnProperty(i) && typeof behaviors[i].attach === 'function') {
          // Don't stop the execution of behaviors in case of an error.
          try {
            behaviors[i].attach(map, mapSettings);
          }
          catch (e) {
            errors.push({ behavior: i, error: e });
          }
        }
      }
      // Once all behaviors have been processed, inform the user about errors.
      if (errors.length) {
        throw errors;
      }
    },
    /**
     * Determine path based on format.
     */
    'relatePath': function (path, basePath) {
      // Check for a full URL or an absolute path.
      if (path.indexOf('://') >= 0 || path.indexOf('/') === 0) {
        return path;
      }
      else {
        return basePath + path;
      }
    },

    /**
     * Render map.
     *
     * Return map object.
     */
    'renderMap': function (mapData) {
      var map = {};

      // Ensure that the handler exists.
      if (mapData.mapHandler !== undefined &&
        Drupal.mapping.mapHandlers[mapData.mapHandler] !== undefined) {
        map = Drupal.mapping.mapHandlers[mapData.mapHandler](mapData);
      }

      return map;
    },

    /**
     * Layer Operations
     *
     * @param layers
     *  Array of layer definitions or rendered layers.
     * @param map
     *  Map definition.
     * @param op
     *  Operation to be done.
     *  Basic operations are:
     *    -'render': will render the layer definition into a layer object the map
     *    can later use.
     *    -'init': adds a rendered layer object to the map definition. It shall be
     *    used before the map is initialized. It is useful when the map will be
     *    initialized with the layer, no need to add the layer later.
     *    -'add': adds a rendered layer object to a map which is already
     *    initialized.
     *    -'render init': the same as operations 'render' + 'init'.
     *    -'render add': the same as operations 'render' + 'add'.
     *
     * @return rendered layers|map definition
     *   return the map definition in case of any initialization operation,
     *   otherwise will return null;
     */
    'processLayers': function (layers, map, op) {
      Drupal.settings.mapping.layers = Drupal.settings.mapping.layers || {};
      var storedLayers = Drupal.settings.mapping.layers;
      var rendered;
      var index;

      // Go through layers handed over and call the relevant
      // layer handler function with the data and the map.
      for (var i in layers) {
        if (layers.hasOwnProperty(i)) {
          // Ensure that the layer handler is available and is defined.
          if (layers[i].layerHandler !== undefined &&
            Drupal.mapping.layerHandlers[layers[i].layerHandler] !== undefined) {
            switch (op) {
              // Render + Init.
              case 'render init':
                // Render.
                rendered = Drupal.mapping.layerHandlers[layers[i].layerHandler](layers[i], map, 'render');
                // Init.
                Drupal.mapping.layerHandlers[layers[i].layerHandler](rendered, map, 'init');
                index = layers[i].name;
                storedLayers[index] = rendered;
                break;

              // Render + Add.
              case 'render add':
                // Render.
                rendered = Drupal.mapping.layerHandlers[layers[i].layerHandler](layers[i], map, 'render');
                // Add.
                Drupal.mapping.layerHandlers[layers[i].layerHandler](rendered, map, 'add');
                index = layers[i].name;
                storedLayers[index].push(rendered);
                break;

              // Any other single operation, such as 'render', or custom operations.
              default:
                rendered = Drupal.mapping.layerHandlers[layers[i].layerHandler](layers[i], map, op);
                index = layers[i].name;
                storedLayers[index].push(rendered);
                break;
            }
          }
        }
      }
    }
  };

  /**
   * Namespace for mapping objects.
   */
  Drupal.mapping.layerHandlers = Drupal.mapping.layerHandlers || {};
  Drupal.mapping.mapHandlers = Drupal.mapping.mapHandlers || {};

}(jQuery, Drupal));
