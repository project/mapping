<?php

/**
 * @file
 * Base class for layer types.
 */

/**
 * Default Mapping Layer Type Class
 *
 * This provides a default layer type class so that layer type plugins can be
 * extended with ctools.  See MappingBaseType class.
 */
class MappingLayerType extends MappingBaseType {
  public $baseType = 'layer';
  public $exportFields = array(
    'name',
    'title',
    'description',
    'data',
    'object_type',
  );
}
