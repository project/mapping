<?php

/**
 * @file
 * Base class for style types.
 */

/**
 * Default Mapping Style Type Class
 *
 * This provides a default style type class so that style type plugins can be
 * extended with ctools.  See MappingBaseType class.
 */
class MappingStyleType extends MappingBaseType {
  public $baseType = 'style';
  public $exportFields = array(
    'name',
    'title',
    'description',
    'data',
    'object_type',
  );
}
