<?php

/**
 * @file
 * Base class for all object types.
 */

/**
 * Default Object Type Class
 *
 * This provides a default object type class that are common for all object
 * types.
 */
class MappingBaseType {
  public $baseType = '';
  public $exportFields = array();

  public $name;
  public $title;
  public $description;
  // This property does not follow coding standards in order to conform with
  // Ctools.
  public $object_type;
  // To be used for CSS.
  public $id;

  /**
   * Constructor to set properties of the object.
   *
   * @param array $export
   *   The export plugin.
   */
  public function __construct($export = array()) {
    // Go through exportable fields to set properties of object.
    foreach ($this->exportFields as $k) {
      if (isset($export->{$k})) {
        $this->{$k} = $export->{$k};
      }
    }

    // Determine any existing options in the data array and set defaults if
    // necessary.
    if (isset($this->data) && is_array($this->data)) {
      $this->data = $this->mergeOptions($this->data, $this->optionsDefault());
    }
    // Create empty data array if it is not set.
    else {
      $this->data = array();
    }
  }

  /**
   * Merge options.
   *
   * @param array $data
   *   A keyed array of object specific options.
   * @param array $defaults
   *   A keyed array of default options.
   *
   * @return array
   *   A keyed array of merged options.
   */
  public function mergeOptions($data, $defaults) {
    $merged = array();
    if (is_array($defaults) && is_array($data)) {
      $merged = $data + $defaults;
    }
    return $merged;
  }

  /**
   * Default options.
   *
   * Default function for intializing any options for the type. If the extended
   * types has options, override this and set here.
   *
   * @return array
   *   A keyed array of options.
   */
  public function defaultOptions() {
    return array();
  }

  /**
   * Options form.
   *
   * Default function for the form to use when creating specific instances of
   * this object.
   *
   * @return array
   *   A Drupal form array.
   */
  public function optionsForm() {
    return array();
  }

  /**
   * Form with the type options.
   *
   * Default function for the form to use for options that are for all objects
   * of this specific type.
   *
   * @return array
   *   A Drupal form array.
   */
  public function typeForm() {
    return array();
  }

  /**
   * Function to create an export for the object instance.
   *
   * @return array
   *   A version of this object which can be saved.
   */
  public function createExport() {
    $return = array();
    foreach ($this->exportFields as $k) {
      $return[$k] = $this->{$k};
    }
    return $return;
  }

  /**
   * Save the export instance to the database.
   *
   * @return bool|int
   *   Success value on saving this export.
   */
  public function save() {
    $table = 'mapping_' . $this->baseType . 's';

    if (!empty($this->name)) {
      return (db_select($table)
          ->fields($table, array('name'))
          ->condition('name', $this->name)
          ->execute()
          ->fetchCol()) ?
        drupal_write_record($table, $this->createExport(), 'name') :
        drupal_write_record($table, $this->createExport());
    }
    return FALSE;
  }

  /**
   * Render the object.
   *
   * @param array $element
   *   Renderable array.
   *
   * This is usually where javascript or other files can be loaded and changes
   * to the main map array can be altered if need.
   */
  public function render(&$element) {
    // No return.
  }
}
