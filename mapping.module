<?php

/**
 * @file
 * Main module file for the mapping module.
 */

/**
 * Implements hook_help().
 */
function mapping_help($path, $arg) {
  switch ($path) {
    case 'admin/help#mapping':
      return '<p>' . t('The Mapping module provides an architectural framework to build (geographical) mapping modules.') . '</p>';

  }
  return NULL;
}

/**
 * Implements hook_ctools_plugin_type().
 *
 * This lets ctools knows about the plugins that are implemented for this
 * module. See the following directory for default classes: includes/classes
 */
function mapping_ctools_plugin_type() {
  return array(
    'mapping_map_types' => array(
      'use hooks' => FALSE,
      'classes' => array('map_type'),
    ),
    'mapping_layer_types' => array(
      'use hooks' => FALSE,
      'classes' => array('layer_type'),
    ),
    'mapping_style_types' => array(
      'use hooks' => FALSE,
      'classes' => array('style_type'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function mapping_theme($existing, $type, $theme, $path) {
  return array(
    'mapping_map' => array(
      'render element' => 'element',
      'file' => 'theme/mapping.theme.inc',
      'template' => 'theme/mapping-map',
    ),
  );
}

/**
 * Implements hook_element_info().
 */
function mapping_element_info() {
  $types['mapping_map'] = array(
    '#theme' => 'mapping_map',
  );
  return $types;
}

/**
 * Build a renderable array given a map name.
 *
 * This function is ideal for using in block contents, as renderable arrays are
 * the preferred way to pass an element. It can also be rendered with the
 * function mapping_render().
 *
 * @see mapping_render()
 *
 * @param string $map_name
 *   The map to be rendered.
 *
 * @return string
 *   The themed map.
 */
function mapping_build_renderable_map($map_name = NULL) {
  $element = array();

  // Loads the map.
  $map = mapping_load_map($map_name);
  if (empty($map)) {
    watchdog('mapping', 'Map %name cannot be loaded.',
      array('%name' => $map_name), WATCHDOG_DEBUG);
    return $element;
  }

  // Adds CSS classes.
  $element = array(
    '#type' => 'mapping_map',
    '#map'  => $map,
    '#attributes' => array(
      'class' => array(
        'mapping-map',
        $map->name,
        $map->object_type,
      ),
    ),
  );

  // Base JS sent first.
  $element['#attached']['js'][] = drupal_get_path('module', 'mapping') . '/js/mapping.js';

  // Render map.
  $map->render($element);

  // Render layers.
  if (is_array($map->layers)) {
    foreach ($map->layers as $k => $layer) {
      $map->layers[$k] = mapping_load_layer($k);
      if (method_exists($map->layers[$k], 'render')) {
        // There is a phpcs warning here. phpcs does not find methods in arrays
        // of objects.
        $map->layers[$k]->render($element);
      }
    }
  }

  // Render styles.
  if (is_array($map->styles)) {
    foreach ($map->styles as $k => $style) {
      $map->styles[$k] = mapping_load_style($k);
    }
  }

  // Render behaviors.
  if (is_array($map->behaviors)) {
    foreach ($map->behaviors as $k => $behavior) {
      $map->behaviors[$k] = mapping_load_behavior($k, $behavior);
    }
  }

  // Create ID.
  // ID's are needed if the same map gets rendered more than once on a page and
  // for referencing the correct HTML div for rendering map. Otherwise classes
  // can be used to style things appropriately.
  // If map ID is not defined create an unique ID.
  $map->id = isset($map->id) ? $map->id : drupal_html_id('mapping-map');

  // Adds map to Drupal.settings
  $settings = array(
    'mapping' => array(
      'maps' => array(
        $map->id => $map,
      ),
    ),
  );
  $element['#attached']['js'][] = array(
    'data' => $settings,
    'type' => 'setting',
  );

  return $element;
}

/**
 * Renders a map as an HTML string.
 *
 * Give a map name it will return the rendered HTML string.
 * In most cases a renderable array is preferred, and can be done by function
 * mapping_build_renderable_map().
 *
 * @see mapping_build_renderable_map()
 *
 * @param string $map_name
 *   The name of the map to be rendered.
 *
 * @return string
 *   The rendered HTML.
 */
function mapping_render($map_name) {
  return render(mapping_build_renderable_map($map_name));
}

/**
 * Get export objects.
 *
 * An abstracted function to get exports easily.
 *
 * @param string $object_type
 *   The type of the export.
 * @param string $load
 *   How to load objects.  The following values will work:
 *     - 'all': All objects.
 *     - 'names': $args will be array of names.
 *     - 'conditions': $args will be an array of conditions.
 *       Specifically we can use 'map_type' => 'example'.
 * @param array $args
 *   Array of names to get specific objects.
 *   See $args param in ctools_export_load_object().
 * @param bool $reset
 *   Boolean on whether to reset cache or not.
 *
 * @internal param $type
 *   String name for type of object to get.
 *
 * @return array
 *   Array of exports.
 *
 * @see ctools_export_load_object()
 * @see mapping_maps_export()
 * @see mapping_layers_export()
 * @see mapping_behaviors_export()
 * @see mapping_styles_export()
 */
function mapping_get_exports($object_type = 'maps', $load = 'all', $args = array(), $reset = FALSE) {
  ctools_include('export');
  $object_type = 'mapping_' . $object_type;

  if ($reset) {
    ctools_export_load_object_reset($object_type);
  }
  return ctools_export_load_object($object_type, $load, $args);
}

/**
 * Get plugin info.
 *
 * An abstracted function to get information on plugins.
 *
 * @param string $object_type
 *   Type of the plugin, default
 * @param null $name
 *   (optional) Plugin name, leave NULL to retrieve all.
 *
 * @return array
 *   Array of plugin info.
 */
function mapping_get_plugins($object_type = 'map_types', $name = NULL) {
  ctools_include('plugins');
  $object_type = 'mapping_' . $object_type;

  $plugins[$object_type] = ctools_get_plugins('mapping', $object_type);

  // If $name is given return the plugin information found.
  if (isset($name) && isset($plugins[$object_type][$name])) {
    return $plugins[$object_type][$name];
  }
  // Plugin type exists, but the given name is not found.
  elseif (isset($name) && !isset($plugins[$object_type][$name])) {
    return array();
  }
  // Return all plugins of the given type.
  return $plugins[$object_type];
}

/**
 * Loads an object from its name or all objects if no name is given.
 *
 * Given a name it will search for a exportable with this name. I the exportable
 * is not found it will try searching by plugin names. If any of them is found
 * then it will load the class and build the object.
 *
 * @param string $object_type
 *   Type of the object. Defaults to 'map'.
 * @param string $name
 *   (Optional) String name of exportable or plugin. If not given it will call
 *   mapping_load_all_objects() to load all objects of the given $object_type.
 * @param bool $reset
 *   Boolean whether to reset cached info.
 *
 * @return object
 *   Loaded object from exportable or plugin.
 *
 * @see mapping_load_all_objects()
 */
function mapping_load_object($object_type = 'map', $name = NULL, $reset = FALSE) {
  // Fallback to load all exports if no name is given.
  if (!isset($name)) {
    return mapping_load_all_objects($object_type, $reset);
  }

  ctools_include('export');
  $export_type = $object_type . 's';
  $plugin_type = $object_type . '_types';
  $class_type = $object_type . '_type';
  $loaded = &drupal_static(__FUNCTION__, array(), $reset);

  // Set the empty object according to its object_type.
  switch ($object_type) {
    case 'map':
      $empty = new MappingMapType();
      break;

    case 'layer':
      $empty = new MappingLayerType();
      break;

    case 'style':
      $empty = new MappingStyleType();
      break;
  }

  // If it is not cached try to get from Ctools exportables or plugins.
  if (!isset($loaded[$object_type][$name])) {
    // Error flags that also hold error messages.
    $export_error = FALSE;
    $plugin_error = FALSE;

    // Try to get export.
    $export = mapping_get_exports($export_type, 'names', array($name), $reset);
    if (isset($export[$name])) {
      $export = $export[$name];
    }
    // Export not found.
    else {
      $export_error = 'Ctools could not get the export %name. ';
    }
    // Invalid export.
    if (!is_object($export) || empty($export->object_type)) {
      $export_error = 'The Ctools export %name is empty or lacks of object type. ';
    }

    // If the export is found then get its plugin searching by its object_type.
    if (empty($export_error)) {
      $plugin_name = $export->object_type;
    }
    // Else try to use $name as a plugin's machine name.
    else {
      $plugin_name = $name;
    }

    // Get plugin.
    $plugin = mapping_get_plugins($plugin_type, $plugin_name);
    if (empty($plugin) || !isset($plugin['plugin type'])) {
      $plugin_error = 'Ctools could not get the plugin type %plugin_type of object type %object_type.';
    }

    // Set watchdog error if cannot get plugin by its name nor by a valid
    // export.
    if (!empty($export_error) && !empty($plugin_error)) {
      watchdog('mapping', $export_error . $plugin_error, array(
          '%name' => $name,
          '%plugin_type' => $plugin_type,
          '%object_type' => $object_type,
        ),
        WATCHDOG_ERROR);
      return $empty;
    }

    // Try to get the object class.
    if ($plugin_class = ctools_plugin_get_class($plugin, $class_type)) {
      $loaded[$object_type][$name] = new $plugin_class($export);
    }
    else {
      watchdog(
        'mapping',
        'Ctools could not get the class from the class identifier %class_type in object %name',
        array(
          '%class_type' => $class_type,
          '%name' => $name,
        ),
        WATCHDOG_ERROR);
      return $empty;
    }
  }

  return isset($loaded[$object_type][$name]) ? $loaded[$object_type][$name] : $empty;
}

/**
 * Load all objects of a given type.
 *
 * Given a type it will load all exports and plugins of that type and load their
 * objects.
 *
 * @param string $object_type
 *   Type of the object. Defaults to 'map'.
 * @param bool $reset
 *   Boolean whether to reset cached info.
 *
 * @return array
 *   Array of loaded objects.
 *
 * @see mapping_load_object()
 */
function mapping_load_all_objects($object_type = 'map', $reset = FALSE) {
  ctools_include('export');
  $empty = array();
  $export_type = $object_type . 's';
  $plugin_type = $object_type . '_types';
  $class_type = $object_type . '_type';
  $loaded = &drupal_static(__FUNCTION__, array(), $reset);

  if (!isset($loaded[$object_type])) {
    // Get objects from exports.
    $exports = mapping_get_exports($export_type, 'all', array(), $reset);
    foreach ($exports as $export) {
      $plugin = mapping_get_plugins($plugin_type, $export->object_type);
      // Try to get the object class.
      if ($plugin_class = ctools_plugin_get_class($plugin, $class_type)) {
        $loaded[$object_type][$export->name] = new $plugin_class($export);
      }
      else {
        watchdog(
          'mapping',
          'Ctools could not get the class from the class identifier %class_type in object %name',
          array(
            '%class_type' => $class_type,
            '%name' => $export->name,
          ),
          WATCHDOG_ERROR);
      }
    }
  }

  return isset($loaded[$object_type]) ? $loaded[$object_type] : $empty;
}

/**
 * Retrieve all map data exports.
 *
 * @param string $map_type
 *   (optional) Type of map to retrieve, leave NULL to retrieve all.
 * @param bool $reset
 *   Boolean on whether to reset cache or not.
 *
 * @return array
 *   Array of exports.
 */
function mapping_maps_export($map_type = NULL, $reset = FALSE) {
  if (!empty($map_type)) {
    return mapping_get_exports('maps', 'conditions', array('map_type', $map_type), $reset);
  }
  else {
    return mapping_get_exports('maps', 'all', array(), $reset);
  }
}

/**
 * Retrieve specific map data export.
 *
 * @param string $name
 *   Name of map export to retrieve.
 * @param bool $reset
 *   Boolean on whether to reset cache or not.
 *
 * @return array
 *   Array of exports.
 */
function mapping_map_export($name, $reset = FALSE) {
  return mapping_get_exports('maps', 'names', array($name), $reset);
}

/**
 * Retrieve all layer data exports.
 *
 * @param string $layer_type
 *   Type of layer to retrieve, leave NULL to retrieve all.
 * @param bool $reset
 *   Boolean on whether to reset cache or not.
 *
 * @return array
 *   Array of exports.
 */
function mapping_layers_export($layer_type = NULL, $reset = FALSE) {
  if (!empty($layer_type)) {
    return mapping_get_exports('layers', 'conditions', array('layer_type' => $layer_type), $reset);
  }
  else {
    return mapping_get_exports('layers', 'all', array(), $reset);
  }
}

/**
 * Retrieve specific layer data export.
 *
 * @param string $name
 *   Name of layer export to retrieve.
 * @param bool $reset
 *   Boolean on whether to reset cache or not.
 *
 * @return array
 *   Array of exports.
 */
function mapping_layer_export($name, $reset = FALSE) {
  return mapping_get_exports('layers', 'names', array($name), $reset);
}

/**
 * Retrieve all style data exports.
 *
 * @param string $style_type
 *   (optional) Type of style to retrieve, leave NULL to retrieve all.
 * @param bool $reset
 *   Boolean on whether to reset cache or not.
 *
 * @return array
 *   Array of exports.
 */
function mapping_styles_export($style_type = NULL, $reset = FALSE) {
  if (!empty($style_type)) {
    return mapping_get_exports('styles', 'conditions', array('style_type' => $style_type), $reset);
  }
  else {
    return mapping_get_exports('styles', 'all', array(), $reset);
  }
}

/**
 * Retrieve specific style data export.
 *
 * @param string $name
 *   Name of style export to retrieve.
 * @param bool $reset
 *   Boolean on whether to reset cache or not.
 *
 * @return array
 *   Array of exports.
 */
function mapping_style_export($name, $reset = FALSE) {
  return mapping_get_exports('styles', 'names', array($name), $reset);
}

/**
 * Retrieve map types plugin info.
 *
 * Provide a name for a specific plugin if desired.
 *
 * @param string $name
 *   (optional) Specific plugin name to retrieve, leave NULL to retrieve all.
 *
 * @return mixed
 *   Array of map type plugin info.
 */
function mapping_map_types_info($name = NULL) {
  return mapping_get_plugins('map_types', $name);
}

/**
 * Retrieve layer types plugin info.
 *
 * Provide a name for a specific plugin if desired.
 *
 * @param string $name
 *   (optional) Specific plugin name to retrieve, leave NULL to retrieve all.
 *
 * @return array
 *   Array of layer type plugin info.
 */
function mapping_layer_types_info($name = NULL) {
  return mapping_get_plugins('layer_types', $name);
}

/**
 * Retrieve style types plugin info.
 *
 * Provide a name for a specific plugin if desired.
 *
 * @param string $name
 *   (optional) Specific plugin name to retrieve, leave NULL to retrieve all.
 *
 * @return array
 *   Array of style type plugin info.
 */
function mapping_style_types_info($name = NULL) {
  return mapping_get_plugins('style_types', $name);
}

/**
 * Retrieve behavior plugin info.
 *
 * Provide a name for a specific plugin if desired.
 *
 * @param string $name
 *   (optional) Specific plugin name to retrieve, leave NULL to retrieve all.
 *
 * @return array
 *   Array of style type plugin info.
 */
function mapping_behaviors_info($name = NULL) {
  return mapping_get_plugins('behaviors', $name);
}

/**
 * Load behavior.
 *
 * @param string $name
 *   (optional) String name for type of object to get, leave NULL to retrieve
 * all.
 * @param bool $reset
 *   Boolean whether to reset cached info.
 *
 * @return object
 *   The loaded object using the class of its the Ctools plugin type.
 */
function mapping_load_behavior($name = NULL, $reset = FALSE) {
  return mapping_load_object('behavior', $name, $reset);
}

/**
 * Load a map, given export.
 *
 * @param string $name
 *   (optional) String name for type of object to get, leave NULL to retrieve
 * all.
 * @param bool $reset
 *   Boolean whether to reset cached info.
 *
 * @return object
 *   The loaded object using the class of its the Ctools plugin type.
 */
function mapping_load_map($name = NULL, $reset = FALSE) {
  return mapping_load_object('map', $name, $reset);
}

/**
 * Load a layer, given export.
 *
 * @param string $name
 *   (optional) String name for type of object to get, leave NULL to retrieve
 * all.
 * @param bool $reset
 *   Boolean whether to reset cached info.
 *
 * @return object
 *   The loaded object using the class of its the Ctools plugin type.
 */
function mapping_load_layer($name = NULL, $reset = FALSE) {
  return mapping_load_object('layer', $name, $reset);
}

/**
 * Load a style, given export.
 *
 * @param string $name
 *   (optional) String name for type of object to get, leave NULL to retrieve
 * all.
 * @param bool $reset
 *   Boolean whether to reset cached info.
 *
 * @return object
 *   The loaded object using the class of its the Ctools plugin type.
 */
function mapping_load_style($name = NULL, $reset = FALSE) {
  return mapping_load_object('style', $name, $reset);
}

/**
 * Implements hook_block_info().
 */
function mapping_block_info() {
  $blocks = mapping_list_map_blocks();
  return $blocks;
}

/**
 * List the maps which have blocks enabled.
 *
 * @return array
 *   Array of map blocks information containing:
 *    -'map_name': The map name.
 *    -'map_title': The map title.
 *    -'info': The block info to be used at mapping_block_info().
 */
function mapping_list_map_blocks() {
  $list = array();

  // Load all maps.
  $maps = mapping_load_map(NULL, FALSE);

  foreach ($maps as $map) {
    // Get the map blocks.
    if (isset($map->data['block']) &&  $map->data['block'] === TRUE) {
      // Prepend the string "block_" to the block delta name.
      $blockid = 'block_' . $map->name;
      $list[$blockid]['map_name'] = $map->name;
      $list[$blockid]['map_title'] = $map->title;
      $list[$blockid]['info'] = t('Mapping block for !map_title', array(
        '!map_title' => $map->title,
      ));
    }
  }

  return $list;
}

/**
 * Implements hook_block_view().
 */
function mapping_block_view($delta = '') {
  $block = array();

  // Get all map blocks.
  $map_blocks = mapping_list_map_blocks();
  // Get the block information for that specific delta.
  if (isset($map_blocks[$delta])) {
    // Set block subject as the map's title.
    $block['subject'] = $map_blocks[$delta]['map_title'];

    // Set teh block content as the renderable array of the map.
    $map_name = $map_blocks[$delta]['map_name'];
    $block['content'] = mapping_build_renderable_map($map_name);
  }

  return $block;
}
