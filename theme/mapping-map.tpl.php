<?php

/**
 * @file
 * Default theme implementation to display a map.
 *
 * Available variables:
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - mapping-map: The default class for Mapping maps.
 *   - $map->name: The name of the map
 *   - $map->object_type: The Ctools plugin for handling this map.
 * - $variables array containing:
 *    - id: ID of this HTML element.
 *    - height: Height of the map.
 *    - width: Width of the map.
 *
 *
 * @see mapping_preprocess_mapping_map()
 * @see mapping_leaflet_mapping_maps()
 */
?>
<div id="<?php print $variables['id']; ?>" class="<?php print $classes; ?>" style="height:<?php print $variables['height']; ?>; width:<?php print $variables['width']; ?>">
</div>
