<?php

/**
 * @file
 * This file holds the theme and preprocess functions for mapping module
 */

/**
 * Preprocess function for mapping_map.
 */
function mapping_preprocess_mapping_map(&$variables, $hook) {
  $map = $variables['element']['#map'];

  $width = (isset($map->data['width'])) ? $map->data['width'] : 'auto';
  $variables['width'] = $width;

  $height = (isset($map->data['height'])) ? $map->data['height'] : '400px';
  $variables['height'] = $height;

  $variables['id'] = $map->id;

  // @todo #attributes are not being attached automatically by Drupal.
  $variables['classes_array'] += $variables['element']['#attributes']['class'];
  return $variables;
}
