<?php

/**
 * @file
 * Provides a layer for the example.
 */

/**
 * Google Layer Class
 *
 * This provides a layer for the example.
 */
class MappingExampleGoogleLayerType extends MappingLayerType {

  public $layerHandler = 'mappingExampleGoogleType';

  /**
   * Default options.
   */
  public function optionsDefault() {
    return array(
      'type' => '',
    );
  }

  /**
   * Options form.
   */
  public function optionsForm() {
    return array(
      'type' => array(
        '#type' => 'select',
        '#title' => t('Type of layer'),
        '#description' => t('The type of Google layer.'),
        '#options' => array(
          'HYBRID' => t('Google Hybrid'),
          'ROADMAP' => t('Google Roadmap'),
          'SATELLITE' => t('Google Satellite'),
          'TERRAIN' => t('Google Terrain'),
        ),
        '#default_value' => $this->data['type'],
      ),
    );
  }

  /**
   * Render the layer.
   */
  public function render(&$element) {
    // Add layer JS.
    $element['#attached']['js'][] = drupal_get_path('module', 'mapping_example') . '/plugins/mapping_layer_types/mapping_example_google_layer_type.js';
  }
}
