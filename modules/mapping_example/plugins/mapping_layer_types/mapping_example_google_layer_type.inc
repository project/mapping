<?php
/**
 * @file
 * Mapping Layer Type plugin for the mapping_example module.
 * Provides a basic layer for Google maps.
 */

/**
 * Define the Ctools plugin options.
 */
$plugin = array(
  'title' => t('Google Layer'),
  'description' => t('Provides a layer type for Google maps.'),
  'map_type' => 'mapping_example_google_map_type',
  'layer_type' => array(
    // This is the file name, not the class itself.
    // File name must end on .class.php, as Ctools standards.
    'class' => 'MappingExampleGoogleLayerType',
  ),
);
