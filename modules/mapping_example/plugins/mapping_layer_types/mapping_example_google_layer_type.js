/**
 * @file
 * Mapping Example Google Map Layer Type
 */

/**
 * Process Google Layers for Mapping Example module
 *
 * @param layerData
 *   Data about the layer.
 * @param map
 *   Reference to map object.
 * @param op
 *   Operation to be taken.
 *   The basic operations are 'render', 'init', 'add'.
 *   Operations 'render init' and 'render add' are controlled by the main JS.
 *   To avoid running operations twice the code should filter the operation type
 *   by using a switch or if statement.
 *   - render: should return the rendered layer as the map can later add/init.
 *     This operation renders the layer in a way it can later be read by the
 *     map library and added to an initialized map.
 *   - init: adds a rendered layer to a map definition, before the map
 *     initialization. This is useful for example in case of initializing the
 *     map with the layers already in the definition.
 *   - add: adds a rendered layer to an initialized map.
 *
 * @return
 *   rendered layer for render operation, returns null for other operations.
 */
Drupal.mapping.layerHandlers.mappingExampleGoogleType = function(layerData, map, op) {
  switch (op) {
    case 'render':
      return google.maps.MapTypeId[layerData.data.type];

    case 'init':
      break;

    case 'add':
      map.setMapTypeId(layerData);
      break;

  }
};
