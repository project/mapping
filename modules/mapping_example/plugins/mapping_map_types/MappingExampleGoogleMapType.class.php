<?php

/**
 * @file
 * Provides a map for the example.
 */

/**
 * Google Map Class
 *
 * This provides a map for the example.
 */
class MappingExampleGoogleMapType extends MappingMapType {

  // The JS function handler.
  public $mapHandler = 'mappingExampleGoogleMapType';

  /**
   * Default options.
   */
  public function optionsDefault() {
    return array(
      'center' => array(0, 0),
      'sensor' => FALSE,
    );
  }

  /**
   * Options form.
   */
  public function optionsForm() {
    return array(
      'center' => array(
        '#type' => 'textfield',
        '#title' => t('Map center'),
        '#description' => t('The default center of the map.'),
        '#options' => array(),
        '#default_value' => $this->data['center'],
      ),
    );
  }

  /**
   * Render the map.
   */
  public function render(&$element) {
    // Build URL.
    $js = 'http://maps.google.com/maps/api/js';
    $js .= ($this->data['sensor']) ? '?sensor=true' : '?sensor=false';
    // Add google JS.
    $element['#attached']['js'][] = array(
      'data' => $js,
      'type' => 'external',
    );

    // Add this plugin's JS.
    $js = drupal_get_path('module', 'mapping_example') . '/plugins/mapping_map_types/mapping_example_google_map_type.js';
    $element['#attached']['js'][] = $js;
  }
}
