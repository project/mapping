/**
 * @file
 * Mapping Example Google Map Type
 */

/**
 * Process Google Layers for Mapping Example module
 *
 * @param mapData
 *   Data about the layer.
 * @return
 *   Map object.
 */
Drupal.mapping.mapHandlers.mappingExampleGoogleMapType = function(mapData) {

  // This is a hack just for demoing purposes\
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(-34.397, 150.644),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById(mapData.id), mapOptions);

  // Add layers.
  Drupal.mapping.processLayers(Drupal.settings.mapping.maps[mapData.id].layers, map, 'render add');
  return map;
};
