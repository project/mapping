<?php

/**
 * @file
 * Mapping Map Type plugin for the mapping_example module. Provides a basic map
 * type for Google maps.
 */

/**
 * Define the Ctools plugin options.
 */
$plugin = array(
  'title' => t('Google Map'),
  'description' => t('Google Map v3 example type.'),
  'map_type' => array(
    // This is the file name, not the class itself.
    // File name must end on .class.php, as Ctools standards.
    'class' => 'MappingExampleGoogleMapType',
  ),
);
